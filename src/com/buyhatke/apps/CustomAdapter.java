package com.buyhatke.apps;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {
	
	private Activity activity;
	static String[][] arr;
	private static LayoutInflater inflater=null;
	ImageDownloader id;
	SharedPreferences sharedPref;
	static boolean loadimages = true;
	
	public CustomAdapter(SearchActivity a, String[][] arr1) {
		
		
		sharedPref = PreferenceManager.getDefaultSharedPreferences(a.getApplicationContext());
		loadimages = sharedPref.getBoolean("pref_images", true);
		a.setRespondToClicks(true);
		activity = a;
		arr = arr1;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		id = new ImageDownloader(a);
	}

	public int getCount() {
		return (arr == null ? 0 : arr.length);
	}

	public Object getItem(int arg0) {
		return arg0;
	}

	public long getItemId(int arg0) {
		return arg0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d("getView call", "getView called at " + position);
		View vi=convertView;
		if(convertView == null) {
			vi = inflater.inflate(R.layout.list_row, null);
	        vi.setMinimumHeight(100);
	        
		}
		TextView name = (TextView) vi.findViewById(R.id.product_name);
        TextView price = (TextView) vi.findViewById(R.id.product_price);
        TextView site = (TextView) vi.findViewById(R.id.product_site);
        //int len = arr[position][0].length();
        name.setText(arr[position][0]);//.substring(0, len));
        price.setText(arr[position][1]);
        site.setText(arr[position][4]);
        ImageView snapshot = (ImageView) vi.findViewById(R.id.snapshot);
        if(loadimages)
        	id.setImage(snapshot, arr[position][3]);
        else 
        	snapshot.setImageResource(R.drawable.notavailable);
        return vi;
	}
	
	

}
