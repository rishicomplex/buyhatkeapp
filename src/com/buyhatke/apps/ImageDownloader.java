package com.buyhatke.apps;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

public class ImageDownloader {
	
	final int REQ_HEIGHT = 80;
	final int REQ_WIDTH = 80;
	Activity activity;
	LruCache<String, Bitmap> cache;
	
	public ImageDownloader(Activity a) {
		activity = a;
		instantiateCache();
	}
	
	void instantiateCache() {
		int memClass = ((ActivityManager) activity.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
		Log.d("value of memClass", "memory is " + memClass);
		int cacheSize = 1024 * 1024 * memClass / 8;
		cache = new LruCache<String, Bitmap>(cacheSize) {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) {
	            return bitmap.getRowBytes() * bitmap.getHeight();
	        }
	    };
	}
	
	public void addBitmapToCache(String url, Bitmap bitmap) {
	    if (getBitmapFromCache(url) == null) {
	        cache.put(url, bitmap);
	    }
	}

	public Bitmap getBitmapFromCache(String url) {
	    return cache.get(url);
	}
	
	public void clearCache() {
		cache.evictAll();
	}
	
	public void setImage(ImageView iv, String url) {
		if(iv == null) return;
		if(createNewTask(iv, url)) {
			//Log.d("new task being created", url);
			Bitmap bmp = getBitmapFromCache(url);
			if(bmp != null) {
				iv.setImageBitmap(bmp);
				return;
			}
			BitmapDownloaderTask tsk = new BitmapDownloaderTask(iv);
			LoadingDrawable ld = new LoadingDrawable(activity.getResources(), BitmapFactory.decodeResource(activity.getResources(), R.drawable.loading), tsk);
			iv.setImageDrawable(ld);
			tsk.execute(url);
		}
		else {
			Log.d("new task refused", url);
		}
	}
	
	boolean createNewTask(ImageView iv, String url) {
		BitmapDownloaderTask bwt = getTask(iv);
		if(bwt == null) {
			return true;
		}
		else {
			if(url.equals(bwt.url)) {
				return false;
			}
			else {
				bwt.cancel(true);
				return true;
			}
		}
	}
	
	BitmapDownloaderTask getTask(ImageView iv) {
		if(iv == null) return null;
		Drawable dr = iv.getDrawable();
		if(dr instanceof LoadingDrawable) {
			LoadingDrawable ld = (LoadingDrawable) dr;
			return ld.getBitmapDownloaderTask();
		}
		else {
			return null;
		}
	}
	
	Bitmap downloadBitmap(String url) {
		
		try{
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
			conn.setConnectTimeout(40000);
            conn.setReadTimeout(40000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            
            /*BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, options);
            int width = options.outWidth;
            int height = options.outHeight;
            Log.d("Image height", String.valueOf(height));
            Log.d("Image width", String.valueOf(width));
            Log.d("Image type", options.outMimeType);
            int scale = 1;
            
            while(width / 2 > REQ_WIDTH || height / 2 > REQ_HEIGHT) {
            	scale *= 2;
            	width /= 2; height /= 2;
            }
            
            if(scale != 1) {
            	Log.d("image too big", "scale = " + scale);
            }
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            
            HttpURLConnection conn2 = (HttpURLConnection) imageUrl.openConnection();
            InputStream is2 = conn2.getInputStream();
            Bitmap bmp = BitmapFactory.decodeStream(is2, null, options);*/
            Bitmap bmp = BitmapFactory.decodeStream(is);
            is.close();
            
            return bmp;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	


	class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
		
		private final WeakReference<ImageView> imageViewReference;
		private String url;
		
		BitmapDownloaderTask(ImageView iv) {
			imageViewReference = new WeakReference<ImageView>(iv);
		}
		
		@Override
		protected Bitmap doInBackground(String... params) {
			url = params[0];
			Bitmap bmp = downloadBitmap(url);
			return bmp;
		}
	
		@Override
		protected void onPostExecute(Bitmap result) {
			if(isCancelled()) {
				result = null;
			}
			if(imageViewReference != null && result != null) {
				final ImageView iv = imageViewReference.get();
				if(this == getTask(iv)) {
					if(iv != null) {
						iv.setImageBitmap(result);
						addBitmapToCache(url, result);
					}
				}
				
			}
		}
		
	}
	
	class LoadingDrawable extends BitmapDrawable {
		
		private final WeakReference<BitmapDownloaderTask> ref;

		public LoadingDrawable(Resources res, Bitmap bitmap, BitmapDownloaderTask task) {
			super(res, bitmap);
			ref = new WeakReference<BitmapDownloaderTask>(task);
		}
		
		public BitmapDownloaderTask getBitmapDownloaderTask() {
			return ref.get();
		}

		
		
	}
}
 