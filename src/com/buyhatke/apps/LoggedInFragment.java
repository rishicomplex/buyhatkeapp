package com.buyhatke.apps;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

public class LoggedInFragment extends Fragment {

	ImageButton searchbutton;
	EditText searchbox;
	private ProfilePictureView profilePictureView;
	private TextView username;
	
	
	private static final int REAUTH_ACTIVITY_CODE = 100;
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
	   
	    public void call(final Session session, final SessionState state, final Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == REAUTH_ACTIVITY_CODE) {
	        uiHelper.onActivityResult(requestCode, resultCode, data);
	    }
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
	    super.onSaveInstanceState(bundle);
	    uiHelper.onSaveInstanceState(bundle);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.logged_in_fragment, container, false);
		
		searchbox = (EditText) v.findViewById(R.id.searchbox);
        searchbutton = (ImageButton) v.findViewById(R.id.button1);
        
        searchbox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_SEARCH) {
					searchbutton.performClick();
					return true;
				}
				return false;
			}
		});
        
        searchbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String str = searchbox.getText().toString();
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
				imm.hideSoftInputFromWindow(searchbox.getWindowToken(), 0);
				callSearchActivity(str);
				}
        });
        
        /*ImageView dbi = (ImageView) v.findViewById(R.id.dashboardimage);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.dashboardex);
        Bitmap resizedbitmap=Bitmap.createScaledBitmap(bmp, width, width, true);
        dbi.setImageBitmap(resizedbitmap);*/
		
		profilePictureView = (ProfilePictureView) v.findViewById(R.id.fb_profile_pic);
		profilePictureView.setCropped(true);
		
		username = (TextView) v.findViewById(R.id.fb_name);
        
		Session session = Session.getActiveSession();
		if(session != null && session.isOpened()) {
			makeMeRequest(session);
		}
        
		return v;
	}
	
	void callSearchActivity(String str) {
    	//a

        Intent i = new Intent(getActivity(), SearchActivity.class);
		Bundle b = new Bundle();
		b.putString("searchterm", str);
		i.putExtras(b);
		startActivity(i);
    }
	
	private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
	    if (session != null && session.isOpened()) {
	        // Get the user's data.
	        makeMeRequest(session);
	    }
	}
	
	private void makeMeRequest(final Session session) {
		Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
			
			public void onCompleted(GraphUser user, Response response) {
				if(session == Session.getActiveSession()) {
					if(user != null) {
						username.setText(user.getName());
						profilePictureView.setProfileId(user.getId());
						
					}
				}
				if (response.getError() != null) {
	                // Handle errors, will do so later.
	            }
			}
		});
		request.executeAsync();
	}
	
	
}
