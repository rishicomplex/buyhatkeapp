package com.buyhatke.apps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class SplashFragment extends Fragment {

	
	ImageButton searchbutton;
	EditText searchbox;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.splash_fragment, container, false);
		
		searchbox = (EditText) v.findViewById(R.id.searchbox);
        searchbutton = (ImageButton) v.findViewById(R.id.button1);
        
        searchbox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_SEARCH) {
					searchbutton.performClick();
					return true;
				}
				return false;
			}
		});
        
        searchbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String str = searchbox.getText().toString();
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
				imm.hideSoftInputFromWindow(searchbox.getWindowToken(), 0);
				callSearchActivity(str);
				}
        });
		
		
		return v;
	}
	
	void callSearchActivity(String str) {
    	//a

        Intent i = new Intent(getActivity(), SearchActivity.class);
		Bundle b = new Bundle();
		b.putString("searchterm", str);
		i.putExtras(b);
		startActivity(i);
    }

}
