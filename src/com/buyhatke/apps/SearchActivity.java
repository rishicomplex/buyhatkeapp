package com.buyhatke.apps;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.buyhatke.apps.RangeSeekBar.OnRangeSeekBarChangeListener;

@SuppressLint("NewApi")
public class SearchActivity extends Activity {
	
	boolean hasheaderview, hasfooterview;
    CustomAdapter adapter;
    ListView list;
    EditText searchbox;
    ImageButton searchbutton;
	String[][] arr;
	RangeSeekBar<Integer> seekBar;
	public boolean respond_to_clicks = false;
	ProgressBar loadingImage;
	@Override
	protected void onResume() {
		super.onResume();
		if(list != null && adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}

	TextView errortext;
	private final int SETTINGS_ID = Menu.FIRST;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search);
        
        list = (ListView) findViewById(R.id.list_view);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header_view = inflater.inflate(R.layout.list_header, null);
        list.addHeaderView(header_view);
        searchbox = (EditText) header_view.findViewById(R.id.searchbox2);
        searchbutton = (ImageButton) header_view.findViewById(R.id.button2);
        
        seekBar = new RangeSeekBar<Integer>(0, 15000, getApplicationContext());
        //seekBar.setNotifyWhileDragging(true);
        seekBar.setSelectedMinValue(1000);
        seekBar.setSelectedMaxValue(8000);
        seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
                public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                        // handle changed range values
                        Log.d("seekbar", "User selected new range values: MIN=" + minValue + ", MAX=" + maxValue);
                        setAdapterThroughSlider(minValue, maxValue);
                }
        });
        
        
        
       

        hasheaderview = true;
        list.addHeaderView(seekBar);
        //seekBar.setVisibility(View.GONE);
        
        list.setFooterDividersEnabled(false);
        list.setFastScrollEnabled(true);
        
        searchbox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_SEARCH) {
					searchbutton.performClick();
					return true;
				}
				return false;
			}
		});
        searchbutton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				
				imm.hideSoftInputFromWindow(searchbox.getWindowToken(), 0);
				String searchterm = searchbox.getText().toString();
				if(!checkNetworkConnection()) {
					setErrorAdapter("No internet connection");
				}
				else {
					new GetSearchResultsTask().execute(searchterm);
					setLoadingAdapter();
				}
			}

        });
        
        loadingImage = (ProgressBar) findViewById(R.id.loadingImage);
        hasfooterview = true;
        errortext = (TextView) findViewById(R.id.error_textview);
        errortext.setVisibility(View.GONE);
        
        
        list.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				try{
					Log.d("listitem click", "click at position " + position);
					//if(!respond_to_clicks) return;
					int posn = position - 2;
					if(posn < 0) return;
					Uri uri = Uri.parse(CustomAdapter.arr[posn][2]);
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(intent);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
				
				
			}
        	  
        	});
        
        
        
        Bundle b = getIntent().getExtras();
		String searchterm = b.getString("searchterm");
		searchbox.setText(searchterm);
		if(!checkNetworkConnection()) {
			setErrorAdapter("No internet connection");
		}
		else {
			new GetSearchResultsTask().execute(searchterm);
			setLoadingAdapter();
		}
		
		
    }
	
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
	    	boolean b = super.onCreateOptionsMenu(menu);
	    	menu.add(0, SETTINGS_ID, 0, "Settings");
	    	return b;
		}
	    
	    

		@Override
		public boolean onMenuItemSelected(int featureId, MenuItem item) {
			switch(item.getItemId()) {
			case SETTINGS_ID : 
				Intent i = new Intent(this, Settings.class);
				startActivity(i);
			}
			
			return super.onMenuItemSelected(featureId, item);
		}
	
	void removeResults() {
		CustomAdapter.arr = null;
		if(adapter == null) {
			adapter = new CustomAdapter(this, null);
			list.setAdapter(adapter);
		}
		else {
			adapter.notifyDataSetChanged();
		}
	}
	
	void setLoadingAdapter() {
		//list.setAdapter(new LoadingAdapter(this));
		removeResults();
		removeErrorText();
		removeRangeHeader();
		addFootLoader();
	}
	
	void addFootLoader() {
		if(hasfooterview) return;
		else {
			hasfooterview = true;
			loadingImage.setVisibility(View.VISIBLE);
		}
	}
	
	void removeFootLoader() {
		if(hasfooterview) {
			hasfooterview = false;
			loadingImage.setVisibility(View.GONE);
		}
		else return;
	}
	
	void addRangeHeader() {
		if(hasheaderview) return;
		else {
			hasheaderview = true;
			seekBar.setVisibility(View.VISIBLE);
		}
		
	}
	
	void addErrorText() {
		errortext.setVisibility(View.VISIBLE);
	}
	
	void removeErrorText() {
		errortext.setVisibility(View.GONE);
	}
	
	void removeRangeHeader() {
		if(hasheaderview) {
			hasheaderview = false;
			seekBar.setVisibility(View.GONE);
			
		}
		else return;
	}
	
	void setErrorAdapter(String txt) {
		removeFootLoader();
		removeResults();
		
		//list.setAdapter(new ErrorAdapter(this, txt));
		errortext.setText(txt);
		addErrorText();
	}
	
	void setRespondToClicks(boolean b) {
		//respond_to_clicks = b;
		//seekBar.setClickable(b);
		//list.setClickable(b);
		if(b && !hasheaderview) {
			//seekBar.setVisibility(View.VISIBLE);
			list.addHeaderView(seekBar);
			hasheaderview = true;
		}
		else if(!b && hasheaderview){
			list.removeHeaderView(seekBar);
			hasheaderview = false;
		}
	}
	
	String getSite(String url) {
		int posn = url.indexOf("www.");
		posn += 4;
		String name;
		int endposn = url.indexOf(".", posn);
		try {
			name = url.substring(posn, endposn);
		}
		catch(Exception e) {
			e.printStackTrace();
			return "Unknown Site";
		}
		char ch = name.charAt(0);
		ch = Character.toUpperCase(ch);
		name = String.valueOf(ch) + name.substring(1);
		return name;
		
	}
	
	void populateList(String json_string) {
		//search results obtained
		try {
			if(json_string.equals("null")) {
				setErrorAdapter("No results");
				return;
			}
			JSONArray jarr = new JSONArray(json_string);
			Log.d("searchResults", "SearchResults = " + String.valueOf(jarr.length()));
			int len = jarr.length();
			//len = jarr.length() > 25 ? 25 : jarr.length();
			arr = new String[len][5];
			JSONObject temp_obj;
			int i;
			for(i = 0; i < arr.length; i++) {
				temp_obj = jarr.getJSONObject(i);
				arr[i][0] = temp_obj.getString("prod");
				arr[i][1] = temp_obj.getString("price");
				arr[i][2] = temp_obj.getString("link");
				arr[i][3] = temp_obj.getString("image");
				arr[i][4] = getSite(temp_obj.getString("link"));
			}
		}
		catch(JSONException e){
			//error in parsing json
			e.printStackTrace();
			arr = new String[0][5];
		}
		
		if(arr == null || arr.length == 0) {
			setErrorAdapter("No results");
			return;
		}
		
		//ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
		//		  android.R.layout.simple_list_item_1, android.R.id.text1, arr);
		int minind = arr.length / 3;
		int maxind = 8 * arr.length / 9;
		int min = getPriceFromString(arr[minind][1]);
		int max = getPriceFromString(arr[maxind][1]);
		int absMin = getPriceFromString(arr[0][1]);
		int absMax = getPriceFromString(arr[arr.length - 1][1]);
		absMax += 100;
		absMin -= 100;
		if(absMin < 0) absMin = 0;
		seekBar.setAbsValues(absMin, absMax);
		seekBar.setSelectedMinValue(min);
		seekBar.setSelectedMaxValue(max);
		setAdapterThroughSlider(min, max);
		/*adapter = new CustomAdapter(this, arr);
		Log.d("debug", "adapter created");
		list.setAdapter(adapter);*/
		
	}
	
	int getPriceFromString(String str) {
		int ind = str.indexOf('.');
		ind += 1;
		while(!Character.isDigit(str.charAt(ind)) || ind > 10) {
			ind++;
		}
		if(ind == 11) {
			return 0;
		}
		str = str.substring(ind);
		int i;
		int len = str.length();
		for(i = 0; i < len; i++) {
			if(!Character.isDigit(str.charAt(i))) {
				break;
			}
		}
		if(i != len) {
			len = i;
		}
		int ret = 0;
		try{
			ret = Integer.parseInt(str.substring(0, len));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	void setAdapterThroughSlider(int minprice, int maxprice) {
		if(arr == null || arr.length == 0) {
			setErrorAdapter("No results");
			return;
		}
		int i;
		int minind = 0, maxind = arr.length - 1;
		for(i = 0; i < arr.length; i++) {
			if(minind == 0 && getPriceFromString(arr[i][1]) >= minprice) {
				minind = i;
			}
			if(maxind == arr.length - 1 && getPriceFromString(arr[i][1]) > maxprice) {
				if(i == 0) {
					setErrorAdapter("No results");
				}
				else {
					maxind = i - 1;
				}
			}
		}
		int len = maxind - minind + 1;
		if(len <= 0) return;
		//maxlength of a list is 500 (too large?)
		if(len > 500) {
			int diff = len - 500;
			maxind -= diff;
			len = 500;
		}
		String[][] newarr = new String[len][5];
		for(i = 0; i < len; i++) {
			newarr[i] = arr[i + minind];
		}
		
		
		removeErrorText();
		removeFootLoader();
		addRangeHeader();
		CustomAdapter.arr = newarr;
		if(adapter == null) {
			adapter = new CustomAdapter(this, newarr);
			list.setAdapter(adapter);
		}
		else{
			adapter.notifyDataSetChanged();
		}
		
		//list.setAdapter(new CustomAdapter(this, newarr));
		
	}
    
    Boolean checkNetworkConnection() {
    	ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
    	if (networkInfo != null && networkInfo.isConnected()) {
    		return true;    	
	    } else {
	        return false;
	    }
    }
    
    String downloadSearchResults(String searchterm) {
    	searchterm = searchterm.replace(" ", "%20");
    	String html = "Not initialized";
    	try {
    		URL url = new URL("http://ppsreejith.net/projects/ifyou/?search=" + searchterm + "&limit=1000");
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestMethod("GET");
    		conn.connect();
    		int responsecode = conn.getResponseCode();
    		Log.d("debug", "Response code of HttpURLConnection is " + responsecode);
    		InputStream is = conn.getInputStream();
    		StringBuilder strbldr = new StringBuilder();
    		BufferedReader br = new BufferedReader(new InputStreamReader(is));
    		String line = "";
    		while((line = br.readLine()) != null) {
    			strbldr.append(line);
    		}
    		is.close();
    		html = strbldr.toString();
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		html = "An Exception was thrown.";    		
    	}
    	return html;
    		
    }
    
    private class GetSearchResultsTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... arg0) {
			try {
				return downloadSearchResults(arg0[0]);
			}
			catch(Exception e) {
				e.printStackTrace();
				return "There was an error.";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			populateList(result);
		}
    	
    	
    }
}




